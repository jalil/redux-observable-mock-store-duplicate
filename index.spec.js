import Rx from 'rxjs/Rx';
import configureMockStore from 'redux-mock-store';
import { createEpicMiddleware } from 'redux-observable';

const someEpic = (actions$) => actions$
    .do(
        e => console.warn('action', e),
        err => console.warn('error', err),
        () => console.warn('end'),
    )
    .filter(() => false);

const epicMiddleware = createEpicMiddleware(someEpic);
const mockStore = configureMockStore([epicMiddleware]);

const please = time => new Promise(resolve => {
    setTimeout(() => {
        resolve();
    }, time);
});

describe('Epics', () => {
    afterEach(() => {
        epicMiddleware.replaceEpic(someEpic);
    });

    it('creates a new local mock store', () => {
        const store = mockStore();
    });

    it('dispatches some action in a new local mock store', async () => {
        const store = mockStore();

        console.warn('before', store.getActions());

        store.dispatch({ type: 'FOO' });

        console.warn('after', store.getActions());

        expect(store.getActions()).toEqual([
            { type: 'FOO' },
        ]);
    });
});
